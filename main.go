package main

import (
	"fmt"
	"os"
)

func baz() error {
	return nil
}

func main() {
	args := os.Args[1:]
	fmt.Println(args)
}
